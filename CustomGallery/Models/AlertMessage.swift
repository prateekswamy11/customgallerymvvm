//
//  AlertMessage.swift
//  CustomGallery
//
//  Created by Pavle Pesic on 7/14/18.
//  Copyright © 2018 Pavle Pesic. All rights reserved.
//
import Photos
struct AlertMessage {
    
    var title = ""
    var body = ""
    
}
class ImageAssets{
    static let shared = ImageAssets()
     var allImages : [PHAssetCollection] = []
}
