//
//  AlbumViewModel.swift
//  CustomGallery
//
//  Created by maximess206 on 23/06/20.
//  Copyright © 2020 Pavle Pesic. All rights reserved.
//

import Foundation
import Photos
protocol ViewControllerDelegateAlbum{
    func sendStatus(isValid : Bool?)
}
class ViewModelAlbums : ViewModelDelegateAlbums{
    var delegate : ViewControllerDelegateAlbum?
    func sendValue() {
            PHPhotoLibrary.checkAuthorizationStatus {
                if $0 {
                     self.delegate?.sendStatus(isValid: true)
                } else {
                     self.delegate?.sendStatus(isValid: false)
                }
        }
    }
}
