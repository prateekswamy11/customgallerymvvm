//
//  PhotosViewController.swift
//  CustomGallery
//
//  Created by Pavle Pesic on 7/14/18.
//  Copyright © 2018 Pavle Pesic. All rights reserved.
//

import UIKit
import Photos

class PhotosViewController: UIViewController {
    
    // MARK: - Outelts
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblViewShowAlbum: UITableView!
    @IBOutlet weak var btnShowAlbumName: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    // MARK: - Vars & Lets
    var selectedCollection: PHAssetCollection?
    var dropDownAlbumName : PHAssetCollection?
    private var photos: PHFetchResult<PHAsset>!
    private var numbeOfItemsInRow = 3
    var albumsName: [PHAssetCollection] = []
    var arraySelectedImgURL = [String]()
    // MARK: - Controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareCollectionView()
        self.fetchImagesFromGallery(collection: self.selectedCollection)
        self.prepareTableView()
        self.btnShowAlbumName.setTitle(selectedCollection?.localizedTitle!, for: .normal)
    }
    
    // MARK: - Private methods
    private func prepareCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib.init(nibName: "PhoteoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhoteoCollectionViewCell")
    }
    
    private func prepareTableView(){
        self.tblViewShowAlbum.delegate = self
        self.tblViewShowAlbum.dataSource = self
        self.tblViewShowAlbum.isHidden = true
    }
    
    @IBAction func multipleSelectImage(_ sender: UIButton) {
        
        if self.btnSelect.isSelected {
            btnSelect.isSelected = false
            btnSelect.setTitle("Select", for: .normal)
            self.collectionView.allowsMultipleSelection = false
            // btnSelect.backgroundColor = UIColor.green
        }
        else {
            btnSelect.isSelected = true
            btnSelect.setTitle("Cancel", for: .normal)
            self.collectionView.allowsMultipleSelection = true
           
            
        }
    }
    
    @IBAction func showTable(_ sender: UIButton) {
        if tblViewShowAlbum.isHidden{
            tblViewShowAlbum.isHidden = false
            
        }else{
            tblViewShowAlbum.isHidden = true
            
        }
        self.view.bringSubview(toFront: self.tblViewShowAlbum)
    }
    private func fetchImagesFromGallery(collection: PHAssetCollection?) {
        DispatchQueue.main.async {
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            if let collection = collection {
                self.photos = PHAsset.fetchAssets(in: collection, options: fetchOptions)
            } else {
                self.photos = PHAsset.fetchAssets(with: fetchOptions)
            }
            self.collectionView.reloadData()
        }
    }
    
}

// MARK: - Exteinsions
// MARK: - UICollectionViewDataSource

extension PhotosViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let photos = photos {
            return photos.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhoteoCollectionViewCell", for: indexPath) as? PhoteoCollectionViewCell
        
        cell?.setImage(photos.object(at: indexPath.row))
        
        return cell!
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension PhotosViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(20, 20, 20, 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (Int(UIScreen.main.bounds.size.width) - (numbeOfItemsInRow - 1) * 6 - 40) / numbeOfItemsInRow
        return CGSize(width: width, height: width)
    }
    
    
}
extension PhotosViewController : UITableViewDelegate,UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.albumsName.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let album = albumsName[indexPath.row]
        cell.textLabel?.text = album.localizedTitle
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedName = self.albumsName[indexPath.row].localizedTitle as! String
        self.dropDownAlbumName = self.albumsName[indexPath.row]
        self.btnShowAlbumName.setTitle(selectedName as String, for: .normal)
        tblViewShowAlbum.isHidden = true
        self.fetchImagesFromGallery(collection: self.dropDownAlbumName)
        
    }
    
}
